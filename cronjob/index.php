<?php
require_once dirname( dirname( __FILE__ ) ) . "/config.php";
require_once dirname( dirname( __FILE__ ) ) . "/env.php";
require_once ABSPATH . 'helper' . DS . 'Route_Cronjob.php';
require_once ABSPATH . 'helper' . DS . 'Response.php';


$path  = isset( $argv[1] ) ? $argv[1] : '';
$route = new Route_Cronjob( $path );
$route->execute();
