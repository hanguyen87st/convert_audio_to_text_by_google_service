<?php
include_once ABSPATH . 'helper' . DS . 'Google_Speech.php';

use \Curl\Curl;

class Subtitle {
    private $_api_url;

    public function __construct() {
        $this->_api_url = "http://test.dev/api/index.php/Subtitle";
    }

    protected function getVideoData() {
        $key   = "get_video_has_not_subtitle";
        $token = hash_hmac( "sha256", "get_video_has_not_subtitle", CRONJOB_SECRET_API );
        $curl  = new Curl();
        $curl->get( $this->_api_url, array(
            'username' => CRONJOB_USERNAME_API,
            'key'      => $key,
            'token'    => $token
        ) );
        $curl->close();
        if ( $curl->error ) {
            error_log( 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage );

            return false;
        } else {
            $response = json_decode( $curl->response );
            if ( $response->error == 0 ) {
                return $response->data;
            } else {
                error_log( $response->message );

                return false;
            }
        }

    }

    protected function updateSubtitle( $video_id, $subtitle ) {
        $key   = "update_subtitle_for_video";
        $token = hash_hmac( "sha256", "update_subtitle_for_video", CRONJOB_SECRET_API );
        $curl  = new Curl();
        $curl->put( $this->_api_url, array(
            'username' => CRONJOB_USERNAME_API,
            'key'      => $key,
            'token'    => $token,
            'subtitle' => json_encode( $subtitle ),
            'video_id' => $video_id
        ) );
        $curl->close();

        if ( $curl->error ) {
            error_log( 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage );

            return false;
        } else {
            $response = json_decode( $curl->response );
            error_log( $response->message );
            if ( $response->error == 0 ) {
                return true;
            } else {
                error_log( $response->message );

                return false;
            }
        }
    }

    public function getSubtitle() {
        $video_data = $this->getVideoData();
        if ( $video_data ) {
            if ( $video_data->video_name ) {
                $google_speech = new Google_Speech();
                $subtitle      = $google_speech->getSubtitle( $video_data->video_name );
                if ( $subtitle ) {
                    $result = $this->updateSubtitle( $video_data->video_id, $subtitle );
                    if ( $result ) {
                        echo "$video_data->video_name :update subtitle successfully!\n";
                        error_log( "$video_data->video_name :update subtitle successfully!" );

                        return true;
                    } else {
                        echo "$video_data->video_name :Update subtitle unsuccessfully!\n";
                        error_log( "$video_data->video_name :Update subtitle unsuccessfully!" );

                        return false;
                    }
                } else {
                    echo "Can not get subtitle of $video_data->video_name!";
                    error_log( "Can not get subtitle of $video_data->video_name!" );

                    return false;
                }
            } else {
                echo "Video/Audio name is empty";
                error_log( "Video/Audio name is empty" );

                return false;
            }
        }

        return true;
    }
}