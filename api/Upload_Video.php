<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/28/17
 * Time: 00:01
 */
include_once ABSPATH . 'helper' . DS . 'Google_Storage.php';
include_once ABSPATH . 'model' . DS . 'Video.php';

class Upload_Video {
    private $_callback;

    public function __construct() {
        $this->_callback = isset( $_GET['callback'] ) ? $_GET['callback'] : '';
    }

    public function index() {
        $page        = isset( $_GET['page'] ) ? $_GET['page'] : 0;
        $limit       = isset( $_GET['limit'] ) ? $_GET['limit'] : 0;
        $video_model = new Video();
        $video_data  = $video_model->getVideos( $page, $limit );
        if ( $video_data ) {
            echo Response::renderResponse( $this->_callback, [
                "error" => 0,
                "data"  => $video_data
            ] );
        } else {
            echo Response::renderResponse( $this->_callback, [
                "error"   => 1,
                "message" => "Audio file list is empty!"
            ] );
        }

    }

    public function create() {
        $video_url   = isset( $_POST['video_url'] ) ? $_POST['video_url'] : "";
        $video_model = new Video();
        if ( empty( $video_url ) ) {
            $allowed_exts = array( "flac" );
            $extension    = pathinfo( $_FILES['video_file']['name'], PATHINFO_EXTENSION );
            if ( $_FILES["video_file"]["type"] == "audio/flac"
                 && in_array( $extension, $allowed_exts ) ) {
                $path             = ABSPATH . "uploads" . DS;
                $actual_file_name = uniqid( "audio_file_" ) . "." . $extension;
                $tmp              = $_FILES['video_file']['tmp_name'];
                if ( move_uploaded_file( $tmp, $path . $actual_file_name ) ) {
                    $google_storage = new Google_Storage();
                    $result_upload  = $google_storage->uploadFile( $actual_file_name, $path . $actual_file_name );
                    if ( $result_upload ) {
                        unlink( $path . $actual_file_name );
                        $result = $video_model->insert( [
                            'video_url'       => Google_Storage::STORAGE_URL . DS . Google_Storage::BUCKET_NAME . DS . $actual_file_name,
                            'video_name'      => $actual_file_name,
                            'subtitle'        => serialize( [] ),
                            'is_got_subtitle' => 0
                        ] );
                        if ( $result ) {
                            echo Response::renderResponse( $this->_callback, [
                                "error"   => 0,
                                "message" => "Upload file successfully!"
                            ] );
                        } else {
                            echo Response::renderResponse( $this->_callback, [
                                "error"   => 1,
                                "message" => "Save data was failed!"
                            ] );
                        }
                    } else {
                        echo Response::renderResponse( $this->_callback, [
                            "error"   => 1,
                            "message" => "Upload file was failed!"
                        ] );
                    }
                } else {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 1,
                        "message" => "Can not save file!"
                    ] );
                }
            } else {
                echo Response::renderResponse( $this->_callback, [
                    "error"   => 1,
                    "message" => "File is invalid!"
                ] );
            }
        } else {
            $pattern = "#^https://storage\.cloud\.google\.com(/.*)?$#";
            if ( preg_match( $pattern, $video_url ) ) {
                $parse_url  = explode( "/", $video_url );
                $video_name = '';
                if ( is_array( $parse_url ) ) {
                    $video_name = $parse_url[ count( $parse_url ) - 1 ];
                }
                $result = $video_model->insert( [
                    'video_url'       => $video_url,
                    'video_name'      => $video_name,
                    'subtitle'        => serialize( [] ),
                    'is_got_subtitle' => 0
                ] );
                if ( $result ) {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 0,
                        "message" => "Save audio url successfully!"
                    ] );
                } else {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 1,
                        "message" => "Save data was failed!"
                    ] );
                }
            } else {
                echo Response::renderResponse( $this->_callback, [
                    "error"   => 1,
                    "message" => "Audio url is invalid!"
                ] );
            }
        }

    }
}