<?php
require_once "../config.php";
require_once "../env.php";
require_once ABSPATH . 'helper' . DS . 'Authentication_Api.php';
require_once ABSPATH . 'helper' . DS . 'Route_Api.php';
require_once ABSPATH . 'helper' . DS . 'Response.php';
require_once ABSPATH . 'model' . DS . 'Database.php';

$method         = $_SERVER['REQUEST_METHOD'];
$authentication = new Authentication_Api( $method );
$callback       = isset( $_GET['callback'] ) ? $_GET['callback'] : '';
if ( $authentication->checkAuthentication() ) {
    $route = new Route_Api( substr( @$_SERVER['PATH_INFO'], 1 ), $method );
    $route->execute();
} else {
    echo Response::renderResponse( $callback, [
        'error'   => 1,
        'message' => 'Can not authenticate!'
    ] );
}
