<?php
include_once ABSPATH . 'model' . DS . 'Video.php';

class Subtitle {
    private $_callback;

    public function __construct() {
        $this->_callback = isset( $_GET['callback'] ) ? $_GET['callback'] : '';
    }

    protected function updateCronjobSubtitle() {
        $subtitle = isset( $_POST['subtitle'] ) ? $_POST['subtitle'] : '';
        $video_id = isset( $_POST['video_id'] ) ? $_POST['video_id'] : 0;
        if ( $subtitle ) {
            $subtitle = json_decode( $subtitle, true );
            if ( is_array( $subtitle ) && count( $subtitle ) > 0 ) {
                $video_model    = new Video();
                $is_exist_video = $video_model->checkExistVideo( $video_id );
                if ( $is_exist_video ) {
                    $subtitle = serialize( $subtitle );
                    $result   = $video_model->update( [
                        'subtitle'        => $subtitle,
                        'is_got_subtitle' => 1
                    ], "id=$video_id" );
                    if ( $result ) {
                        echo Response::renderResponse( $this->_callback, [
                            "error"   => 0,
                            "message" => "Update subtitle successfully!"
                        ] );
                    } else {
                        echo Response::renderResponse( $this->_callback, [
                            "error"   => 1,
                            "message" => "Update subtitle unsuccessfully!"
                        ] );
                    }
                } else {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 1,
                        "message" => "Can not found video!"
                    ] );
                }
            } else {
                echo Response::renderResponse( $this->_callback, [
                    "error"   => 1,
                    "message" => "Subtitle is invalid format!"
                ] );
            }
        } else {
            echo Response::renderResponse( $this->_callback, [
                "error"   => 1,
                "message" => "Subtitle can not empty!"
            ] );
        }
    }

    protected function updateFrontEndSubtitle() {
        $transcript_id = isset( $_POST['transcript_id'] ) ? $_POST['transcript_id'] : 0;
        $transcript    = isset( $_POST['transcript'] ) ? $_POST['transcript'] : '';
        $video_id      = isset( $_POST['video_id'] ) ? $_POST['video_id'] : 0;

        $video_model = new Video();
        $video_data  = $video_model->getVideoById( $video_id );
        if ( $video_data ) {
            $video_data = $video_data[0];
            $subtitle   = unserialize( $video_data['subtitle'] );
            if ( isset( $subtitle[ $transcript_id ] ) ) {
                $subtitle[ $transcript_id ]['transcript'] = $transcript;
                $subtitle                                 = serialize( $subtitle );
                $result                                   = $video_model->update( [
                    'subtitle' => $subtitle
                ], "id=$video_id" );
                if ( $result ) {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 0,
                        "message" => "Update subtitle successfully!"
                    ] );
                } else {
                    echo Response::renderResponse( $this->_callback, [
                        "error"   => 1,
                        "message" => "Update subtitle unsuccessfully!"
                    ] );
                }
            } else {
                echo Response::renderResponse( $this->_callback, [
                    "error"   => 1,
                    "message" => "Can not found subtitle!"
                ] );
            }
        } else {
            echo Response::renderResponse( $this->_callback, [
                "error"   => 1,
                "message" => "Can not found video!"
            ] );
        }
    }

    public function index() {
        $video_model = new Video();
        $video_data  = $video_model->getVideoByIsGotSubtitle( 0 );

        if ( $video_data ) {
            $video_data = $video_data[0];
            //update getting subtitle is running
            $video_model->updateIsGotSubtitleById( $video_data['id'], 2 );
            echo Response::renderResponse( $this->_callback, [
                "error" => 0,
                "data"  => [
                    'video_id'   => $video_data['id'],
                    'video_name' => $video_data['video_name']
                ]
            ] );
        } else {
            echo Response::renderResponse( $this->_callback, [
                "error"   => 1,
                "message" => "Can not found Video!"
            ] );
        }

    }

    public function update() {
        $transcript_id = isset( $_POST['transcript_id'] ) ? $_POST['transcript_id'] : 0;
        if ( $transcript_id > 0 ) {
            $this->updateFrontEndSubtitle();
        } else {
            $this->updateCronjobSubtitle();
        }

    }

}