<?php
include_once "header.php";
$key   = "load_video";
$token = hash_hmac( "sha256", $key, FRONTEND_SECRET_API );
?>
    <script type="text/javascript" src="../assets/js/video_list.js"></script>
    <div class="container">
        <input type="hidden" name="key" id="key" value="<?= $key ?>">
        <input type="hidden" name="token" id="token" value="<?= $token ?>">
        <input type="hidden" name="username" id="username" value="<?= FRONTEND_USERNAME_API ?>">
        <h2>Audio List</h2>
        <p id="mrk_notice"></p>
        <table class="table">
            <thead>
            <tr>
                <th>Play</th>
                <th>Textscript</th>

            </tr>
            </thead>
            <tbody id="mrk_tbody_file_list">

            </tbody>
        </table>
    </div>
<?php
include_once "footer.php";
?>