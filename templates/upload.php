<?php
include_once "header.php";
$key = "upload_video";
$token = hash_hmac("sha256",$key,FRONTEND_SECRET_API);
?>
    <script type="text/javascript" src="../assets/js/upload_page.js"></script>
    <div class="container">
        <div class="row">
            <h2>Upload Audio</h2>
        </div>
        <div class="row">
            <form class="form-horizontal" id="form_upload" enctype="multipart/form-data" method="post" action="http://test.dev/api/index.php/Upload_Video" >
                <input type="hidden" name="key" id="key" value="<?=$key?>">
                <input type="hidden" name="token" id="token" value="<?=$token?>">
                <input type="hidden" name="username" id="username" value="<?=FRONTEND_USERNAME_API?>">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="video_url">Audio Url:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="video_url" id="video_url" placeholder="Enter audio url">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="video_file">Upload audio (FLAC - sample rate 48000):</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control findDocumentOnboarding" name="video_file" id="video_file" placeholder="Enter video file">
                    </div>
                </div>
                <div class="form-group mrk_notice">
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default " id="mrk_btn_submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
include_once "footer.php";
?>