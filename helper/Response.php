<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/29/17
 * Time: 01:24
 */

class Response {
    public static function renderResponse( $callback, $response_data ) {
        if ( ! is_array( $response_data ) ) {
            $response_data = [];
        }
        if ( $callback ) {
            return $callback . "(" . json_encode( $response_data ) . ")";
        } else {
            return json_encode( $response_data );
        }
    }
}