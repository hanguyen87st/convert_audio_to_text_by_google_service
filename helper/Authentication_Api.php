<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/27/17
 * Time: 22:54
 */
include_once "Route_Api.php";

class Authentication_Api {
    protected $client_list;
    protected $method;

    public function __construct( $method ) {
        $this->client_list = array(
            "frontend" => FRONTEND_SECRET_API,
            "cronjob"  => CRONJOB_KEY
        );
        $this->method      = $method;
    }

    public function checkAuthentication() {
        $client_data = Route_Api::getClientData( $this->method );
        $username    = isset( $client_data['username'] ) ? $client_data['username'] : '';

        if ( ! isset( $this->client_list[ $username ] ) ) {
            return false;
        } else {
            $public_key   = isset( $client_data['key'] ) ? $client_data['key'] : '';
            $client_token = isset( $client_data['token'] ) ? $client_data['token'] : '';
            $server_token = hash_hmac( "sha256", $public_key, $this->client_list[ $username ] );
            if ( $client_token == $server_token ) {
                return true;
            } else {
                return false;
            }
        }
    }
}
