<?php

use Google\Cloud\Speech\SpeechClient;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\Speech\Operation;

class Google_Speech {
    const PROJECT_GOOGLE_ID = PROJECT_GOOGLE_ID;
    const GOOGLE_KEY = ABSPATH . 'helper' . DS . 'google_key.json';
    const BUCKET_NAME = 'manage-video';

    private $_speech;
    private $_options;

    public function __construct( $languageCode = 'en-US' ) {
        $this->_speech  = new SpeechClient( [
            'keyFilePath'  => self::GOOGLE_KEY,
            'projectId'    => self::PROJECT_GOOGLE_ID,
            'languageCode' => $languageCode,
        ] );
        $this->_options = [
            'encoding'              => 'FLAC',
            'sampleRateHertz'       => 48000,
            'enableWordTimeOffsets' => true
        ];

    }

    public function getSubtitle( $file_name ) {

        // Fetch the storage object
        $storage = new StorageClient();
        $object  = $storage->bucket( self::BUCKET_NAME )->object( $file_name );

        // Create the asyncronous recognize operation
        $operation = $this->_speech->beginRecognizeOperation(
            $object,
            $this->_options
        );

        // Wait for the operation to complete
        $backoff = new ExponentialBackoff( 10 );
        $backoff->execute( function () use ( $operation ) {
            print( 'Waiting for operation to complete' . PHP_EOL );
            $operation->reload();
            if ( ! $operation->isComplete() ) {
                throw new Exception( 'Job has not yet completed', 500 );
            }
        } );

        if ( $operation->isComplete() ) {
            $results              = $operation->results();
            $subtitle_information = [];
            $id                   = 0;
            foreach ( $results as $result ) {
                $id ++;
                $alternative                 = $result->alternatives()[0];
                $start_time                  = $alternative['words'][0]['startTime'];
                $end_time                    = $alternative['words'][ count( $alternative['words'] ) - 1 ]['endTime'];
                $subtitle_information[ $id ] = [
                    "id"         => $id,
                    "transcript" => $alternative['transcript'],
                    "confidence" => $alternative['confidence'],
                    "start_time" => $start_time,
                    "end_time"   => $end_time
                ];
            }

            return $subtitle_information;
        } else {
            return false;
        }

    }


}
