<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/28/17
 * Time: 23:25
 */

use Google\Cloud\Storage\StorageClient;

class Google_Storage {
    const PROJECT_GOOGLE_ID = PROJECT_GOOGLE_ID;
    const BUCKET_NAME = 'manage-video';
    const GOOGLE_KEY = ABSPATH . 'helper' . DS . 'google_key.json';
    const STORAGE_URL = "https://storage.cloud.google.com";

    private $_storage;

    public function __construct() {
        $this->_storage = new StorageClient( [
            'keyFilePath' => self::GOOGLE_KEY,
            'projectId'   => self::PROJECT_GOOGLE_ID
        ] );
    }

    public function uploadFile( $destination_file_name, $source_file ) {
        try {
            $file   = fopen( $source_file, 'r' );
            $bucket = $this->_storage->bucket( self::BUCKET_NAME );
            $object = $bucket->upload( $file, [
                'name' => $destination_file_name
            ] );

            return true;
        } catch ( Exception $e ) {
            return false;
        }
    }
}
