<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/29/17
 * Time: 01:11
 */

class Route_Cronjob {
    public $controller;
    protected $path;

    public function __construct( $path ) {
        $this->path = $path;
    }


    public function execute() {
        if ( $this->path ) {
            $request = explode( "/", $this->path );
            if ( is_array( $request ) && count( $request ) > 0 ) {
                $class_name   = $request[0];
                $require_file = ABSPATH . "cronjob" . DS . $class_name . ".php";
                if ( file_exists( $require_file ) ) {
                    require_once $require_file;

                    if ( class_exists( $class_name ) ) {
                        $this->controller = new $class_name();
                        $action_name      = isset( $request[1] ) ? $request[1] : '';
                        if ( $action_name && method_exists( $this->controller, $action_name ) ) {
                            $this->controller->$action_name();
                        } else {
                            echo "Can not find action name!";
                        }
                    } else {
                        echo 'Can not find controller!';
                    }
                }
            }
        } else {
            echo 'Can not find cronjob task!';
        }
    }
}