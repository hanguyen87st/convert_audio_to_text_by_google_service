<?php

class Route_Api {
    public $controller;

    protected $method;
    protected $path;

    public function __construct( $path, $method ) {
        $this->method = $method;
        $this->path   = $path;
    }


    public function execute() {
        $callback = isset( $_GET['callback'] ) ? $_GET['callback'] : '';
        if ( $this->path ) {
            $request = explode( "/", $this->path );
            if ( is_array( $request ) && count( $request ) > 0 ) {
                $class_name   = $request[0];
                $require_file = ABSPATH . "api" . DS . $class_name . ".php";
                if ( file_exists( $require_file ) ) {
                    require_once $require_file;

                    if ( class_exists( $request[0] ) ) {
                        $this->controller = new $class_name();

                        switch ( $this->method ) {
                            case 'POST':
                                if ( method_exists( $this->controller, "create" ) ) {
                                    $this->controller->create();
                                } else {
                                    echo Response::renderResponse( $callback, [
                                        'error'   => 1,
                                        'message' => 'Can not find "create" action!'
                                    ] );
                                }
                                break;
                            case 'PUT':
                            case 'PATCH':
                                if ( method_exists( $this->controller, "update" ) ) {
                                    $this->controller->update();
                                } else {
                                    echo Response::renderResponse( $callback, [
                                        'error'   => 1,
                                        'message' => 'Can not find "update" action!'
                                    ] );
                                }
                                break;
                            case 'DELETE':
                                if ( method_exists( $this->controller, "delete" ) ) {
                                    $this->controller->delete();
                                } else {
                                    echo Response::renderResponse( $callback, [
                                        'error'   => 1,
                                        'message' => 'Can not find "delete" action!'
                                    ] );
                                }
                                break;
                            case 'GET':
                                if ( method_exists( $this->controller, "index" ) ) {
                                    $this->controller->index();
                                } else {
                                    echo Response::renderResponse( $callback, [
                                        'error'   => 1,
                                        'message' => 'Can not find "index" action!'
                                    ] );
                                }
                                break;
                            default:
                                echo Response::renderResponse( $callback, [
                                    'error'   => 1,
                                    'message' => 'This method http is not supported!'
                                ] );
                                break;
                        }
                    } else {
                        echo Response::renderResponse( $callback, [
                            'error'   => 1,
                            'message' => 'Can not find controller!'
                        ] );
                    }
                }
            }
        } else {
            echo Response::renderResponse( $callback, [ 'error' => 1, 'message' => 'Can not parse this link!' ] );
        }
    }

    public static function getClientData( $method ) {
        switch ( $method ) {
            case 'POST':
                $client_data = $_POST;
                break;
            case 'PUT':
            case 'PATCH':
            case 'DELETE':
                parse_str( file_get_contents( 'php://input' ), $client_data );
                if ( is_array( $client_data ) ) {
                    foreach ( $client_data as $k => $v ) {
                        $_POST[ $k ] = $v;
                    }
                    $client_data = $_POST;
                }
                break;
            case 'GET':
                $client_data = $_GET;
                break;
            default:
                $client_data = [];
                break;
        }

        return $client_data;
    }
}