<?php

class Route {

    public function __construct() {
    }

    public static function getCurrentPagePath( $get_param ) {
        if ( is_array( $get_param ) && isset( $get_param['page'] ) ) {
            $page      = strtolower( trim( $get_param['page'] ) );
            $page_path = '';
            switch ( $page ) {
                case 'video_list':
                    $page_path = "templates/video_list.php";
                    break;
                case 'upload':
                    $page_path = "templates/upload.php";
                    break;
                default:
                    $page_path = "templates/upload.php";
                    break;
            }

            return $page_path;
        } else {
            return "templates/home.php";
        }
    }

}