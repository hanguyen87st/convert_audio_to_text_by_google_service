<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/28/17
 * Time: 18:20
 */
include_once "Connection.php";

class Database {
    protected $table_name;
    protected $connection;
    protected $query;

    public function __construct( $table_name ) {
        $this->table_name = $table_name;
        $this->connection = Connection::getInstance()->getConnection();
    }

    protected function log_error( $sql ) {
        error_log( "Error: Our query failed to execute and here is why:" );
        error_log( "Query: " . $sql );
        error_log( "Errno: " . $this->connection->errno );
        error_log( "Error: " . $this->connection->error );
    }

    public function insert( $data ) {
        $columns = "";
        $values  = "";
        foreach ( $data as $column => $value ) {
            $columns .= ( $columns == "" ) ? "" : ", ";
            $columns .= $column;
            $values  .= ( $values == "" ) ? "" : ", ";
            $values  .= "'" . addslashes( $value ) . "'";
        }
        $sql = "insert into $this->table_name ($columns) values ($values)";
        if ( isset( $this->connection ) ) {
            if ( ! ( $this->query = $this->connection->query( $sql ) ) ) {
                $this->log_error( $sql );

                return false;
            }

            return $this->query;
        } else {
            trigger_error( 'MySQLConnection not opened', E_USER_ERROR );

            return false;
        }
    }

    public function update( $data, $where ) {
        if ( isset( $this->connection ) ) {
            foreach ( $data as $column => $value ) {
                $sql = "UPDATE $this->table_name SET $column = '" . addslashes( $value ) . "' WHERE $where";
                if ( ! @$this->connection->query( $sql ) ) {
                    return false;
                }
            }

            return true;
        } else {
            trigger_error( 'MySQLConnection not opened', E_USER_ERROR );

            return false;
        }
    }

    public function select( $select = '*', $where ) {
        if ( isset( $this->connection ) ) {
            $sql = "SELECT " . $select . " FROM $this->table_name WHERE $where";
            if ( ( $result = $this->connection->query( $sql ) ) ) {
                if ( $result->num_rows > 0 ) {
                    $this->query = $result;
                    $return_data = [];
                    while ( $data = $result->fetch_assoc() ) {
                        $return_data[] = $data;
                    }

                    return $return_data;
                }

                return false;
            }

            return false;
        } else {
            trigger_error( 'MySQLConnection not opened', E_USER_ERROR );

            return false;
        }
    }

}