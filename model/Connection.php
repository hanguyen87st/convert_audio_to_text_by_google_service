<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/28/17
 * Time: 23:56
 */

class Connection {
    private $_connection;
    private static $_instance; //The single instance
    private $_host = "localhost";
    private $_username = "root";
    private $_password = "root";
    private $_database = "test";

    public static function getInstance() {
        if ( ! self::$_instance ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    private function __construct() {
        $this->_connection = new mysqli( $this->_host, $this->_username,
            $this->_password, $this->_database );

        if ( mysqli_connect_error() ) {
            trigger_error( "Failed to connect to MySQL: " . mysql_connect_error(),
                E_USER_ERROR );
        }
    }

    private function __clone() {
    }

    public function getConnection() {
        return $this->_connection;
    }

    public function Close() {
        $this->_connection->close();
        self::$_instance = null;
    }
}