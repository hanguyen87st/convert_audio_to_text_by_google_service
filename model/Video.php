<?php
/**
 * Created by PhpStorm.
 * User: nguyenha
 * Date: 11/28/17
 * Time: 18:23
 */

class Video extends Database {
    public function __construct() {
        parent::__construct( "video" );
    }

    public function getVideoByIsGotSubtitle( $is_got_subtitle ) {
        return $this->select( "*", "is_got_subtitle=$is_got_subtitle LIMIT 1" );
    }

    public function getVideos( $page, $limit ) {
        $offset            = ( $page - 1 ) * $limit;
        $video_object_list = $this->select( "*", "is_got_subtitle=1 LIMIT $offset,$limit" );
        if ( $video_object_list ) {
            foreach ( $video_object_list as &$video ) {
                $video['subtitle'] = unserialize( $video['subtitle'] );
            }

            return $video_object_list;
        } else {
            return false;
        }
    }

    public function updateIsGotSubtitleById( $id, $is_got_subtitle ) {
        return $this->update( [ 'is_got_subtitle' => $is_got_subtitle ], "id=$id" );
    }

    public function getVideoById( $id ) {
        return $this->select( "*", "id=$id LIMIT 1" );
    }

    public function checkExistVideo( $id ) {
        if ( $this->getVideoById( $id ) ) {
            return true;
        } else {
            return false;
        }
    }
}