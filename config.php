<?php
error_reporting( E_ALL );
ini_set( 'display_errors', 0 );
ini_set( 'log_errors', 1 );
ini_set( 'error_log', 'debug.log' );
define( 'ABSPATH', dirname( __FILE__ ) . '/' );
define( 'DS', DIRECTORY_SEPARATOR );
require ABSPATH . 'vendor/autoload.php';