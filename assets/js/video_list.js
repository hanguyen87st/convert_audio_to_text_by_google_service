jQuery(document).ready(function ($) {
    load_video();

    function load_video() {
        var data = {
            'page': 1,
            'limit': 20,
            'key': $("#key").val(),
            'token': $("#token").val(),
            'username': $("#username").val()
        };

        $.ajax({
            url: "http://test.dev/api/index.php/Upload_Video",
            type: 'get',
            dataType: 'jsonp',
            jsonp: "callback",
            data: data,
            success: function (results) {
                if (results.error == 0) {
                    $.each(results.data, function (index, value) {
                        var html = '<tr>\n' +
                            '                <td>\n' +
                            '                    <audio class="mrk_play_' + value.id + '" name="testClip" controls="" preload="auto"\n' +
                            '                           src="' + value.video_url + '">\n' +
                            '                        <source src="flac.flac">\n' +
                            '                        Your browser does not support the audio tag.\n' +
                            '                    </audio>\n' +
                            '                </td>\n' +
                            '                <td>\n' +
                            '                    <div id="subtitles-container" class="show-sub">\n';
                        $.each(value.subtitle, function (k, v) {
                            html += '                       <span> <span class="mrk_transcription" id="subscript_' + value.id + '_' + v.id + '" data-start="' + v.start_time + '" data-video-id="' + value.id + '">\n' +
                                '                            ' + v.transcript + '</span>\n' +
                                '                            <div class="savep"><div class="button_save" data-video-id="' + value.id + '" data-transcript-id="' + v.id + '">Edit</div></div>\n' +
                                '                        </span>\n';
                        });


                        html += '                    </div>\n' +
                            '                </td>\n' +
                            '            </tr>';
                        $("#mrk_tbody_file_list").append(html);
                    });
                } else {
                    $("#mrk_notice").html(results.message);
                }
            }
        });
    }

    $(document).on("click", '.button_save', function (event) {
        var btn = $(this);
        var video_id = btn.data("video-id");
        var transcript_id = btn.data("transcript-id");
        if (btn.hasClass("editing")) {
            var transcript = $('#subscript_' + video_id + '_' + transcript_id + ' textarea').val();
            var data = {
                'transcript': transcript,
                'video_id': video_id,
                'transcript_id': transcript_id,
                'key': $("#key").val(),
                'token': $("#token").val(),
                'username': $("#username").val()
            };

            $.ajax({
                url: "http://test.dev/api/index.php/Subtitle",
                type: 'put',
                dataType: 'jsonp',
                jsonp: "callback",
                data: data,
                success: function (results) {
                    $('#subscript_' + video_id + '_' + transcript_id).html(transcript);
                    btn.removeClass("editing");
                    btn.html("Edit");
                }
            });

        } else {
            btn.addClass("editing");
            btn.html("Update");
            var transcription = $('#subscript_' + video_id + '_' + transcript_id).html();
            $('#subscript_' + video_id + '_' + transcript_id).html('<textarea  rows="2" cols="100">' + transcription.trim() + '</textarea>');
        }


    });

    $(document).on("click", '.mrk_transcription', function (event) {
        console.log("DEBUG");
        var start_time = $(this).data("start");
        var video_id = $(this).data("video-id");
        $('.mrk_play_' + video_id)[0].currentTime = parseFloat(start_time);
        $('.mrk_play_' + video_id)[0].play();
    });


});