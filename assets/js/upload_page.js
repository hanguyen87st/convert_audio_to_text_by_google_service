jQuery(document).ready(function ($) {
    $("#form_upload").on("submit", function (event) {
        event.preventDefault();
        $('.mrk_notice').html("LOADING......");
        $('#mrk_btn_submit').prop("disabled", true);
        var form = $('#form_upload')[0];
        var data = new FormData(form);
        $.ajax({
            url: "http://test.dev/api/index.php/Upload_Video",
            type: 'post',
            enctype: 'multipart/form-data',
            dataType: 'jsonp',
            jsonp: "callback",
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (results) {
                $('#mrk_btn_submit').prop("disabled", false);
                $('.mrk_notice').html(results.message);
            }
        });
    });

});